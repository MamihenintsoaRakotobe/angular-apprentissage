import { Component, OnInit } from '@angular/core';
import {IHero} from "./hero";
import { HEROES } from './mock-hero';
import {HeroService} from "../hero.service";

@Component({
  selector: 'app-heroes',
  templateUrl: './heroes.component.html',
  styleUrls: ['./heroes.component.css']
})
export class HeroesComponent implements OnInit {

  public heroes:IHero[] = [];

  selectedHero?: IHero;
  onSelect = (hero: IHero): void => {
    this.selectedHero = hero;
  }


  constructor(private heroService:HeroService) {}

  getHeroes = (): void => {
    //this.heroService.getHeroes().subscribe(heroes => this.heroes = heroes);
    this.heroService.getHeroes().toPromise()
      .then((heroes) => this.heroes = heroes)
      .catch(e => console.log(e));
  }

  ngOnInit(): void {
    this.getHeroes();
  }

}
