import { Injectable } from '@angular/core';
import {HEROES} from "./heroes/mock-hero";
import {IHero} from "./heroes/hero";
import { Observable, of } from 'rxjs';
import {MessageService} from "./message.service";

@Injectable({
  providedIn: 'root'
})
export class HeroService {

  constructor(private messageService: MessageService) { }
  getHeroes = ():Observable<IHero[]> => {
    const heroes = of(HEROES);
    this.messageService.add('HeroService: fetched heroes');
    console.log('====>hero',heroes);
    return heroes;
  }
}
